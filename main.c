#include "fillit.h"

static void     free_tetris(t_etris *s)
{
    free(s);
}

static void     fillit(char *filename)
{
    t_etris          *s;

    s = (t_etris *)malloc(sizeof(t_etris));

    tetris_init(s, filename);
    /*
    if (error(filename))
    {
        putcolor("error", BOLD_RED, 2, 1);
        return ;
    }
    putcolor("Clear", BOLD_GREEN, 1, 1);
    */
    free_tetris(s);
}

int             main(int ac, char **av)
{
    if (ac == 2)
        fillit(av[1]);
    return (0);
}
