
#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include "colors.h"
# include <stdlib.h>
# include <unistd.h>

typedef struct      s_etris
{
    char            **map;
    int             fd;
}                   t_etris;

int					error(char *fd);
void                tetris_init(t_etris *s, char *name);

#endif
