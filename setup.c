#include "fillit.h"

static void					save(t_etris *s)
{
    char    *temp;
    int     i;
	int		line;

    i = 0;
	line = 0;
    while (get_next_line(s->fd, &temp))
    {
		if (line != 4)
		{
			if (!s->map[i])
				s->map[i] = ft_strdup(temp);
			else
			{
				if (line != 0)
					s->map[i] = ft_strcat(s->map[i], "\n");
				s->map[i] = ft_strcat(s->map[i], temp);
			}
			line++;
		}
		else
		{
			i++;
			s->map[i] = ft_strcat(s->map[i], "\n\n");
			i++;
			line = 0;
		}
	}
    close(s->fd);
}

static void			init_map(t_etris *s)
{
	int		i;

	i = 0;
	s->map = (char **)malloc(sizeof(char *) * 27);
	while (i < 26)
	{
		s->map[i] = (char *)malloc(sizeof(char) * 21);
		ft_bzero(s->map[i], 21);
		i++;
	}
}

void				tetris_init(t_etris *s, char *name)
{
	int		i = 0;
    s->fd = open(name, O_RDONLY);
    if (s->fd == -1)
    {
        putcolor("can't open file", BOLD_RED, 2, 1);
        return ;
    }
	init_map(s);
	save(s);
	while (s->map[i] && s->map[i][0] != 0)
	{
		ft_putstr(s->map[i]);
		i++;
	}
	/*
	while (s->map[i] && s->map[i][0] != 0)
	{
		j = 0;
		while (s->map[i][j])
		{
			ft_putchar(s->map[i][j]);
			if (j == 3 || j == 7 || j == 11 || j == 15)
			{
				if (j == 15 && s->map[i + 1][0] == 0)
					break;
				else
					ft_putchar('\n');
			}
			j++;
		}
		//ft_putchar('\n');
		i++;
	}
	*/
}
